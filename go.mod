module cwtch.im/cwtch

go 1.17

require (
	git.openprivacy.ca/cwtch.im/tapir v0.6.0
	git.openprivacy.ca/openprivacy/connectivity v1.8.6
	git.openprivacy.ca/openprivacy/log v1.0.3
	github.com/gtank/ristretto255 v0.1.3-0.20210930101514-6bb39798585c
	github.com/mutecomm/go-sqlcipher/v4 v4.4.2
	github.com/onsi/ginkgo/v2 v2.1.4
	github.com/onsi/gomega v1.20.1
	golang.org/x/crypto v0.0.0-20220826181053-bd7e27e6170d
)

require (
	filippo.io/edwards25519 v1.0.0 // indirect
	git.openprivacy.ca/openprivacy/bine v0.0.4 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/gtank/merlin v0.1.1 // indirect
	github.com/mimoo/StrobeGo v0.0.0-20220103164710-9a04d6ca976b // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/net v0.0.0-20220826154423-83b083e8dc8b // indirect
	golang.org/x/sys v0.0.0-20220825204002-c680a09ffe64 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
