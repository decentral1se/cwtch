package plugins

import (
	"testing"
	"time"

	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/protocol/connections"
	"git.openprivacy.ca/openprivacy/log"
)

// TestContactRetryQueue simulates some basic connection queueing
// NOTE: This whole test is a race condition, and does flag go's detector
// We are invasively checking the internal state of the retry plugin and accessing pointers from another
// thread.
// We could build an entire thread safe monitoring functonality, but that would dramatically expand the scope of this test.
func TestContactRetryQueue(t *testing.T) {
	log.SetLevel(log.LevelDebug)
	bus := event.NewEventManager()
	cr := NewConnectionRetry(bus, "").(*contactRetry)
	cr.ACNUp = true          // fake an ACN connection...
	cr.protocolEngine = true // fake protocol engine
	go cr.run()

	t.Logf("contact plugin up and running..sending peer connection...")
	// Assert that there is a peer connection identified as "test"
	bus.Publish(event.NewEvent(event.QueuePeerRequest, map[event.Field]string{event.RemotePeer: "test", event.LastSeen: "test"}))

	// Wait until the test actually exists, and is queued
	// This is the worst part of this test setup. Ideally we would sleep, or some other yielding, but
	// go test scheduling doesn't like that and even sleeping long periods won't cause the event thread to make
	// progress...
	for {
		if pinf, exists := cr.connections.Load("test"); exists {
			if pinf.(*contact).queued {
				break
			}
		}
	}

	pinf, _ := cr.connections.Load("test")
	if pinf.(*contact).queued == false {
		t.Fatalf("test connection should be queued, actually: %v", pinf.(*contact).queued)
	}

	// Asset that "test" is authenticated
	cr.handleEvent("test", connections.AUTHENTICATED, peerConn)

	// Assert that "test has a valid state"
	pinf, _ = cr.connections.Load("test")
	if pinf.(*contact).state != 3 {
		t.Fatalf("test connection should be in authenticated after update, actually: %v", pinf.(*contact).state)
	}

	// Publish an unrelated event to trigger the Plugin to go through a queuing cycle
	// If we didn't do this we would have to wait 30 seconds for a check-in
	bus.Publish(event.NewEvent(event.PeerStateChange, map[event.Field]string{event.RemotePeer: "test2", event.ConnectionState: "Disconnected"}))
	time.Sleep(time.Second)
	if pinf.(*contact).queued != false {
		t.Fatalf("test connection should not be queued, actually: %v", pinf.(*contact).queued)
	}

	// Publish a  new peer request...
	bus.Publish(event.NewEvent(event.QueuePeerRequest, map[event.Field]string{event.RemotePeer: "test"}))
	time.Sleep(time.Second) // yield for a second so the event can catch up...

	// Peer test should be forced to queue....
	pinf, _ = cr.connections.Load("test")
	if pinf.(*contact).queued != true {
		t.Fatalf("test connection should be forced to queue after new queue peer request")
	}

	cr.Shutdown()
}
