package peer

import (
	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/model"
	"cwtch.im/cwtch/model/attr"
	"cwtch.im/cwtch/settings"
)

type ProfileHooks interface {
	// EventsToRegister returns a set of events that the extension is interested hooking
	EventsToRegister() []event.Type

	// ExperimentsToRegister returns a set of experiments that the extension is interested in being notified about
	ExperimentsToRegister() []string

	// OnEvent is called whenever an event Registered with RegisterEvents is called
	OnEvent(event event.Event, profile CwtchPeer)

	// OnContactRequestValue is Hooked when a contact sends a request for the given path
	OnContactRequestValue(profile CwtchPeer, conversation model.Conversation, eventID string, path attr.ScopedZonedPath)

	// OnContactReceiveValue is Hooked after a profile receives a response to a Get/Val Request
	OnContactReceiveValue(profile CwtchPeer, conversation model.Conversation, path attr.ScopedZonedPath, value string, exists bool)

	// NotifySettingsUpdate allow profile hooks to access configs e.g. download folder
	NotifySettingsUpdate(settings settings.GlobalSettings)
}

type ProfileHook struct {
	extension   ProfileHooks
	events      map[event.Type]bool
	experiments map[string]bool
}

func ConstructHook(extension ProfileHooks) ProfileHook {
	events := make(map[event.Type]bool)
	for _, e := range extension.EventsToRegister() {
		events[e] = true
	}

	experiments := make(map[string]bool)
	for _, experiment := range extension.ExperimentsToRegister() {
		experiments[experiment] = true
	}

	return ProfileHook{
		extension,
		events,
		experiments,
	}
}
